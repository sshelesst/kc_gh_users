import { Logger } from 'tslog'

const logger = new Logger({
  displayFilePath: 'hidden',
  displayLoggerName: false,
  displayInstanceName: false,
  displayFunctionName: false,
})

export const info = (...args: unknown[]): void => {
  logger.info(...args)
}

export const warn = (...args: unknown[]): void => {
  logger.warn(...args)
}

export const error = (...args: unknown[]): void => {
  logger.error(...args)
}
