import { AxiosError } from 'axios'
import cors from 'cors'
import express, { Express, NextFunction, Request, Response } from 'express'
import path from 'path'
import { router } from './resources/github/github.router'
import * as log from './services/logger.service'

export const app: Express = express()

app.use(
  cors({
    origin: 'https://kc-gh-users-v2.karpovdns.net',
  }),
)
app.use(express.json())
app.use(express.static(path.join(__dirname, 'static')))
app.use('/api', router)
app.use('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'static', 'index.html'))
})
app.use((err: NodeJS.ErrnoException | AxiosError, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof AxiosError) {
    const code = err.response?.status || 500
    log.error(`${code}: ${err.message}`)
    return res.status(code).json({ response: 'error', message: err.message })
  }

  log.error(`${err.code}: ${err.message}`)
  return res.status(500).json({ response: 'error', message: err.message })
})
