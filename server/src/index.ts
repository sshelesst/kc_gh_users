import * as dotenv from 'dotenv'
import path from 'path'
dotenv.config({ path: path.join(__dirname, '../.env') })

import fs from 'fs'
import https, { Server } from 'https'
import { app } from './app'
import * as log from './services/logger.service'

const privateKey = fs.readFileSync('/etc/letsencrypt/live/kc-gh-users-v2.karpovdns.net/privkey.pem', 'utf8')
const certificate = fs.readFileSync('/etc/letsencrypt/live/kc-gh-users-v2.karpovdns.net/cert.pem', 'utf8')
const ca = fs.readFileSync('/etc/letsencrypt/live/kc-gh-users-v2.karpovdns.net/chain.pem', 'utf8')

const credentials = {
  key: privateKey,
  cert: certificate,
  ca: ca,
}

const PORT = process.env.PORT || 3002
const server: Server = https.createServer(credentials, app)

async function startServer() {
  server.listen(PORT, () => {
    log.info(`Сервер запущен в режиме ${process.env.NODE_ENV} на https://localhost:${PORT}`)
  })
}

startServer()
