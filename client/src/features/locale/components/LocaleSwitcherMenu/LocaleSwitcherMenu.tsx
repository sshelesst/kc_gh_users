import './LocaleSwitcherMenu.css'
import cn from 'classnames'
import React, { FC } from 'react'
import { Locale } from '../../types'

interface ILocaleSwitcherMenu {
  selectedLocale: Locale
  onChangeLocale: (locale: Locale) => void
  className?: string
}

export const LocaleSwitcherMenu: FC<ILocaleSwitcherMenu> = ({ selectedLocale, onChangeLocale, className }) => {
  return (
    <div className={cn('locale-switcher-menu', className)}>
      <button
        className={cn('locale-switcher-menu__option', {
          'locale-switcher-menu__option--active': selectedLocale === Locale.ru,
        })}
        onClick={() => onChangeLocale(Locale.ru)}
      >
        <span className="locale-switcher-menut__text">Русский</span>
      </button>
      <button
        className={cn('locale-switcher-menu__option', {
          'locale-switcher-menu__option--active': selectedLocale === Locale.en,
        })}
        onClick={() => onChangeLocale(Locale.en)}
      >
        <span className="locale-switcher-menut__text">English</span>
      </button>
    </div>
  )
}
