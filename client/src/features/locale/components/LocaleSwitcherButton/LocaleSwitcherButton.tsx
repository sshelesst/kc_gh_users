import './LocaleSwitcherButton.css'
import cn from 'classnames'
import React, { ForwardedRef, forwardRef } from 'react'
import { useTranslation } from 'react-i18next'
import { Arrow } from '../../../../components/Icons/Arrow'
import { Locale } from '../../types'

interface ILocaleSwitcherButton {
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void
  locale: Locale
  opened?: boolean
}

export const LocaleSwitcherButton = forwardRef(function LocaleSwitcherButton(
  { onClick, locale, opened }: ILocaleSwitcherButton,
  ref: ForwardedRef<HTMLButtonElement>,
) {
  const { t } = useTranslation()
  return (
    <button
      className={cn('locale-switcher-button', { 'locale-switcher-button--opened': opened })}
      ref={ref}
      onClick={onClick}
      aria-label={t('aria_layout_switcher') as string}
    >
      <span className="locale-switcher-button__text">
        {locale === 'en' && 'EN'}
        {locale === 'ru' && 'RU'}
      </span>
      <span className="locale-switcher-button__icon">
        <Arrow />
      </span>
    </button>
  )
})
