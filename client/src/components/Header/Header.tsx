import './Header.css'
import React, { FC, FormEvent, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useNavigate, useSearchParams } from 'react-router-dom'
import { LocaleSwitcher } from '../../features/locale/components/LocaleSwitcher/LocaleSwitcher'

export const Header: FC = () => {
  const [searchValue, setSearchValue] = useState('')
  const [query, setQuery] = useState<string | null>(null)
  const [page, setPage] = useState('users')
  const [searchParams] = useSearchParams()
  const navigate = useNavigate()
  const location = useLocation()
  const { t } = useTranslation()

  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    if (!searchValue.trim().length) return

    navigate({
      pathname: '/search',
      search: `query=${searchValue}`,
    })
  }

  React.useEffect(() => {
    const usersReg = new RegExp('^(/|/users/?)$')
    const userReg = new RegExp('^/users/([a-zd](?:[a-zd]|-(?=[a-zd])){0,38})$', 'i')
    const searchReg = new RegExp('^/search/?$')

    let nickname

    switch (true) {
      case usersReg.test(location.pathname):
        setPage('users')
        break
      case searchReg.test(location.pathname):
        setPage('search')
        break
      case userReg.test(location.pathname):
        nickname = location.pathname.match(userReg)
        if (nickname) setPage(nickname[1])
        break
      default:
        setPage('users')
    }
  }, [location])

  React.useEffect(() => {
    setQuery(searchParams.get('query'))
  }, [searchParams])

  React.useEffect(() => {
    query ? setSearchValue(query) : setSearchValue('')
  }, [query])

  return (
    <header className="header">
      <div className="container header__container">
        <nav className="header__navigation">
          <ul className="header__navigation-list">
            <li className="header__navigation-list-item">
              <Link to="/" className="header__navigation-link" aria-label={t('aria_home') as string}>
                {t('header')}
              </Link>
            </li>
            {page !== 'users' && (
              <li className="header__navigation-list-item">
                <a className="header__navigation-link header__navigation-link--user">
                  {page !== 'search' ? page : t('header_search_caption')}
                </a>
              </li>
            )}
          </ul>
        </nav>
        <div className="header__controls">
          <LocaleSwitcher />
          <div className="header__search">
            <form
              className="header__search-form"
              onSubmit={onSubmit}
              aria-label={t('aria_form_of_searching') as string}
            >
              <input
                type="search"
                className="header__search-input"
                placeholder={t('header_input_placeholder') as string}
                value={searchValue}
                onChange={event => setSearchValue(event.currentTarget.value)}
              />
              <button type="submit" className="header__search-button">
                {t('header_search_btn')}
              </button>
            </form>
          </div>
        </div>
      </div>
    </header>
  )
}
