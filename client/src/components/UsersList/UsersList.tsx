import './UsersList.css'
import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { IUser } from '../../utils/types/api.types'
import { Loading } from '../Loading/Loading'

interface Props {
  users: IUser[] | null
  onClick: (id: string) => void
}

export const UsersList: FC<Props> = ({ users, onClick }) => {
  const { t } = useTranslation()

  return (
    <>
      {users ? (
        users.length ? (
          <div className="users-list">
            {users.map(item => (
              <section className="users-list__item" key={item.login} aria-label={t('aria_card_of_user') as string}>
                <div className="users-list__image-container">
                  <img
                    className="users-list__image"
                    src={item.avatar_url}
                    alt={`${t('alt_profile_photo')} ${item.login}`}
                  />
                </div>
                <div className="users-list__content">
                  <h2 className="users-list__title">
                    <Link to={`/users/${item.login}`} className="link" onClick={() => onClick(item.login)}>
                      {item.login}
                    </Link>
                  </h2>
                  <p className="users-list__text">{t('users_list_repos', { count: item.public_repos })}</p>
                  <p className="users-list__text">{item.organization ? item.organization : <br />}</p>
                </div>
              </section>
            ))}
          </div>
        ) : null
      ) : (
        <Loading />
      )}
    </>
  )
}
