import React, { FC } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { getUsers } from '../../utils/helpers/fetch.helper'
import { IUser } from '../../utils/types/api.types'
import { Header } from '../Header/Header'
import { UserProfilePage } from '../UserProfilePage/UserProfilePage'
import { UsersPage } from '../UsersPage/UsersPage'
import { UsersSearchPage } from '../UsersSearchPage/UsersSearchPage'

export const App: FC = () => {
  const [users, setUsers] = React.useState<IUser[] | null>(null)
  const [user, setUser] = React.useState<IUser | null>(null)

  React.useEffect(() => {
    getUsers().then(response => setUsers(response))
  }, [])

  const onUserClick = (id: string) => {
    const found = users?.find(item => item.login === id)

    found ? setUser(found) : setUser(null)
  }

  return (
    <>
      <Header />
      <Routes>
        <Route path="/users/:id" element={<UserProfilePage userData={user} />} />
        <Route path="/users?" element={<UsersPage users={users} onUserClick={onUserClick} />} />
        <Route path="/search" element={<UsersSearchPage onUserClick={onUserClick} />} />
        <Route path="/*" element={<Navigate to="/" />} />
      </Routes>
    </>
  )
}
