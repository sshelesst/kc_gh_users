import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { useSearchParams } from 'react-router-dom'
import { getUsers } from '../../utils/helpers/fetch.helper'
import { IUser } from '../../utils/types/api.types'
import { UsersList } from '../UsersList/UsersList'

interface Props {
  onUserClick: (id: string) => void
}

export const UsersSearchPage: FC<Props> = ({ onUserClick }) => {
  const [searchParams] = useSearchParams()
  const [users, setUsers] = React.useState<IUser[] | null>(null)
  const { t } = useTranslation()

  const query = searchParams.get('query')

  React.useEffect(() => {
    setUsers(null)
    if (query) getUsers(query).then(response => setUsers(response))
  }, [query])

  return (
    <main aria-label={t('aria_list_of_users') as string}>
      <div className="container">
        <h1 className="title">
          {!users
            ? null
            : users.length
            ? `${t('users_search_page_found')} ${query}`
            : `${t('users_search_page_not_found')} ${query}`}
        </h1>
        <UsersList users={users} onClick={onUserClick} />
      </div>
    </main>
  )
}
