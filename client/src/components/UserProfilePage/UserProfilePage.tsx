import './UserProfilePage.css'
import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { getRepositories, getUserById } from '../../utils/helpers/fetch.helper'
import { formatNumbers } from '../../utils/helpers/format.helper'
import { IRepository, IUser } from '../../utils/types/api.types'
import { NotFoundPage } from '../NotFoundPage/NotFoundPage'

interface Props {
  userData: IUser | null
}

export const UserProfilePage: FC<Props> = ({ userData }) => {
  const { id }: { id?: string } = useParams()
  const [repositories, setRepositories] = React.useState<IRepository[]>([])
  const [user, setUser] = React.useState<IUser | null>(null)
  const [error, setError] = React.useState(false)
  const { t } = useTranslation()

  React.useEffect(() => {
    if (!userData) {
      if (id)
        getUserById(id)
          .then(response => {
            setUser(response)
          })
          .catch(() => {
            setError(true)
          })
    } else {
      setUser(userData)
    }
  }, [])

  React.useEffect(() => {
    if (!user || !user.public_repos) return
    getRepositories(user.login).then(response => setRepositories(response))
  }, [user])

  return (
    <>
      {!error ? (
        user && (
          <main aria-label={t('aria_user_page') as string}>
            <div className="container">
              <section className="user-profile" aria-label={t('aria_card_of_user') as string}>
                <div className="user-profile__image-container">
                  <img
                    className="user-profile__image"
                    src={user.avatar_url}
                    alt={`${t('alt_profile_photo')} ${user.login}`}
                  />
                </div>
                <div className="user-profile__content" aria-label={t('aria_user_profile_description') as string}>
                  <h1 className="user-profile__title">
                    {user.name ? `${user.name},` : ''} <span className="user-profile__accent">{user.login}</span>
                  </h1>
                  <p className="user-profile__text">
                    <span className="user-profile__accent">{formatNumbers(user.followers)}</span>{' '}
                    {t('users_profile_page_followers', { count: user.followers })} ·{' '}
                    <span className="user-profile__accent">{formatNumbers(user.following)}</span>{' '}
                    {t('users_profile_page_following', { count: user.following })}
                    {user.blog && (
                      <>
                        <span> · </span>
                        <a href={user.blog} className="link" target="_blank" rel="noreferrer">
                          {user.blog}
                        </a>
                      </>
                    )}
                  </p>
                </div>
              </section>

              <section className="repository-list" aria-label={t('aria_list_of_repositories') as string}>
                <div className="repository-list__header">
                  <h2 className="repository-list__title">{t('users_profile_page_repos')}</h2>
                  {repositories.length > 0 && (
                    <a
                      href={`https://github.com/${user.login}?tab=repositories`}
                      className="link"
                      target="_blank"
                      rel="noreferrer"
                    >
                      {t('users_profile_page_repos_link')}
                    </a>
                  )}
                </div>

                <div className="repository-list__container">
                  {repositories.length > 0 &&
                    repositories.map(item => (
                      <section
                        className="repository-list__item"
                        key={item.name}
                        aria-label={t('aria_repository_info') as string}
                      >
                        <h3 className="repository-list__item-title">
                          <a href={item.html_url} className="link" target="_blank" rel="noreferrer">
                            {item.name}
                          </a>
                        </h3>
                        <p className="repository-list__item-text">{item.description}</p>
                      </section>
                    ))}
                </div>
              </section>
            </div>
          </main>
        )
      ) : (
        <NotFoundPage />
      )}
    </>
  )
}
