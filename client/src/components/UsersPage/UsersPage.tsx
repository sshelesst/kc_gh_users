import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { IUser } from '../../utils/types/api.types'
import { UsersList } from '../UsersList/UsersList'

interface Props {
  users: IUser[] | null
  onUserClick: (id: string) => void
}

export const UsersPage: FC<Props> = ({ users, onUserClick }) => {
  const { t } = useTranslation()

  return (
    <>
      <main aria-label={t('aria_list_of_users') as string}>
        <div className="container">
          <UsersList users={users} onClick={onUserClick} />
        </div>
      </main>
    </>
  )
}
