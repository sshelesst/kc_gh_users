import './common.css'
import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter as Router } from 'react-router-dom'
import { App } from './components/App/App'
import { initI18n } from './features/locale/utils'

const rootElement = document.getElementById('root')

if (!rootElement) throw new Error('На странице отсутствует root элемент')
const root = createRoot(rootElement)

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function () {
    navigator.serviceWorker
      .register('/sw.js')
      .then(function () {
        console.log('Service Worker Registered!!')
      })
      .catch(error => {
        console.error('cant register SW', error)
      })
  })
}

initI18n(() => {
  root.render(
    <Router>
      <App />
    </Router>,
  )
})
