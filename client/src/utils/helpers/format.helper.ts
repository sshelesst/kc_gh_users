export const defineCases = (n: number, o: { one: string; little: string; many: string; showNum: boolean }): string => {
  if ((n % 100 >= 10 && n % 100 <= 14) || (n % 100) % 10 === 0) return `${o.showNum ? n : ''} ${o.many}`.trim()

  return `${o.showNum ? n : ''} ${n % 10 === 1 ? o.one : n % 10 >= 5 ? o.many : o.little}`.trim()
}

export const formatNumbers = (n: number) => {
  return n >= 1000 ? `${(n / 1000).toFixed(1)}k` : n
}
