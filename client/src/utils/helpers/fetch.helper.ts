import { CONSTANTS } from '../constants/app.constants'
import { IRepository, IUser } from '../types/api.types'

export const getUsers = async (query?: string): Promise<IUser[]> => {
  return query
    ? await fetchWrapper(`${CONSTANTS.api}/search/users?q=${query}`)
    : await fetchWrapper(`${CONSTANTS.api}/users`)
}

export const getUserById = async (id: string): Promise<IUser> => {
  return await fetchWrapper(`${CONSTANTS.api}/users/${id}`)
}

export const getRepositories = async (id: string): Promise<IRepository[]> => {
  return await fetchWrapper(`${CONSTANTS.api}/users/${id}/repos?sort=pushed`)
}

const fetchWrapper = async <T>(url: string, config: RequestInit = {}): Promise<T> => {
  return await fetch(url, config).then<T>(response => {
    if (!response.ok) {
      if (response.status === 404) throw new Error('Page not found')
    }
    return response.json()
  })
}
