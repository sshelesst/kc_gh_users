const HtmlWebpackPlugin = require('html-webpack-plugin')
const { join } = require('path')
const CONSTANTS = require('../constants')

const setupHtml = () => ({
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: join(CONSTANTS.SOURCE_DIRECTORY, 'index.html'),
      excludeChunks: ['sw'],
    }),
  ],
})

module.exports = { setupHtml }
