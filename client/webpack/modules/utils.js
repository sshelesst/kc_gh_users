const { HotModuleReplacementPlugin } = require('webpack')
const WebpackBar = require('webpackbar')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const ESLintPlugin = require('eslint-webpack-plugin')
const StyleLintPlugin = require('stylelint-webpack-plugin')
const Dotenv = require('dotenv-webpack')

const esLint = () => ({
  plugins: [
    new ESLintPlugin({
      files: '**/*.{tsx,ts,js}',
      fix: true,
      lintDirtyModulesOnly: true,
    }),
  ],
})

const styleLint = () => ({
  plugins: [
    new StyleLintPlugin({
      files: 'src/**/*.?(s)css',
      fix: true,
      lintDirtyModulesOnly: true,
    }),
  ],
})

const connectToHMR = () => ({
  plugins: [new HotModuleReplacementPlugin()],
})

const connectToProgressIndicator = () => ({
  plugins: [new WebpackBar({})],
})

const connectToBundleAnalyzer = () => ({
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'disabled',
      openAnalyzer: false,
      generateStatsFile: true,
    }),
  ],
})

const connectToEnvVariables = () => ({
  plugins: [new Dotenv({ path: './.env' })],
})

module.exports = {
  esLint,
  styleLint,
  connectToHMR,
  connectToProgressIndicator,
  connectToBundleAnalyzer,
  connectToEnvVariables,
}
